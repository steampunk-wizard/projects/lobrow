import os
from pathlib import Path
from tempfile import NamedTemporaryFile, TemporaryDirectory
from unittest import TestCase
from unittest.mock import Mock
from unittest.mock import patch
from io import StringIO

from wizlib.input_handler import InputHandler
from wizlib.config_handler import ConfigHandler

from lobrow import LoBrowApp
from lobrow.command.start_command import StartCommand

FULL_CONFIG = """
lobrow:
  default: foobar
  sites:
    foobar:
      dirpath: here/it/is
      port: 9001
"""


class TestCommandStart(TestCase):

    # def test_default(self):
    #     c = StartCommand(config=ConfigHandler.fake(lobrow_default='foo'))
    #     n = c.execute()
    #     self.assertEqual(c.status, f"Started foo")

    def test_full_config(self):
        with TemporaryDirectory() as tempdir:
            with open(Path(tempdir) / '.lobrow.yml', 'w') as file:
                file.write(FULL_CONFIG)
                file.seek(0)
            path = os.getcwd()
            try:
                os.chdir(tempdir)
                with patch('sys.stdout', o := StringIO()):
                    with patch('sys.stderr', e := StringIO()):
                        LoBrowApp.run('start', debug=True)
            finally:
                os.chdir(path)
            e.seek(0)
            self.assertIn('Started here/it/is on port 9001', e.read())

    # def test_input(self):
    #     x = "hello"
    #     c = DefaultCommand(input=InputHandler.fake(x))
    #     n = c.execute()
    #     self.assertEqual(n, 'Value hello')

    # def test_app(self):
    #     with patch('sys.stdout', o := StringIO()):
    #         with patch('sys.stderr', e := StringIO()):
    #             LoBrowApp.run('default', debug=True)
    #     o.seek(0)
    #     self.assertTrue(o.read().startswith('Value'))
    #     e.seek(0)
    #     self.assertTrue(e.read().startswith('Done'))

# LoBrow

Browse local files with a local server. Read documentation on an airplane.

## Contents

* [Indices and tables](#indices-and-tables)
  * [General Index](#general-index)
  * [Module Index](#module-index)
  * [Search](#search)

## Indices and tables

* [General Index](#general-index)
* [Module Index](#module-index)
* [Search](#search)
